import argparse
import math
import numpy
import pybullet as p
from time import sleep
from typing import Tuple, List
from scipy.spatial.transform import Rotation

#Variables globales que l'on peut changer si on veut une autre modélisation
dt = 0.01 #Délai entre chaque mouvement
longueurs = (4.5, 6.5, 8.7) #Longueur de chacune des pattes
AMPLITUDE = 6 #Hauteur max que lève la patte lorsqu'il marche
d = 16 #rayon du cercle que font les pattes de centre le corps du robot

#Fonction qui renvoie une valeur entre -1 et 1 pour arccos et arcsin
def ensemble_def(valeur):
    return max(-1,min(1,valeur))

#La classe  de la pate
class Pate:
    #Initialisation des pates
    def __init__(self, id: int, longueurs: Tuple[float, float, float], translation: numpy.ndarray, rotation: numpy.ndarray):
        self._longueurs = longueurs
        self._translation = translation
        self._rotation = rotation
    
    #Conversion de la position en coordonnée (x,y,z) en angle pour les moteurs (sigma1,sigma2,sigma3)
    def angles_pour_pos(self, position: numpy.ndarray) -> Tuple[float, float, float]:
        position = position.dot(self._rotation) - self._translation
        
        x = position[0]
        y = position[1]
        z = position[2]
        sigma_1 = 0
        if math.sqrt(pow(x-1,2)+pow(y-1,2)+pow(z,2)) > 19.7 :
            return 0,0,0
        if x!=0 or y==0: #Car atan2 n'est pas défini lorsque x=0 et y=0
            sigma_1 = math.atan2(y, x)
        sigma_1 = max(-2*math.pi/3,min(2*math.pi/3,sigma_1)) #Permet de bloquer une collision évidente entre les pattes du robot
        ma2 = (x - self._longueurs[0] * math.cos(sigma_1)) ** 2 + (y - self._longueurs[0] * math.sin(sigma_1)) ** 2 + z ** 2
        
        sigma_3 = math.pi - math.acos(ensemble_def( (self._longueurs[1] ** 2 + self._longueurs[2] ** 2 - ma2) / (2 * self._longueurs[1] * self._longueurs[2])))
        
        sigma_2 = -math.asin(ensemble_def(z / math.sqrt(ma2)))\
                  + math.acos(ensemble_def( (ma2 + self._longueurs[1] ** 2 - self._longueurs[2] ** 2) / (2 * math.sqrt(ma2) * self._longueurs[1])))
        return sigma_1, sigma_2, sigma_3

#La classe du robot 
class Quadrupede:
    #Initialisation du robot
    def __init__(self):
        translation = numpy.array([4, 0, 0])
        
        self.pate1 = Pate(1, longueurs, translation, Rotation.from_rotvec([0, 0, math.pi / 4]).as_matrix())
        self.pate2 = Pate(2, longueurs, translation, Rotation.from_rotvec([0, 0, math.pi * 3 / 4]).as_matrix())
        self.pate3 = Pate(3, longueurs, translation, Rotation.from_rotvec([0, 0, math.pi * -3 / 4]).as_matrix())
        self.pate4 = Pate(4, longueurs, translation, Rotation.from_rotvec([0, 0, math.pi * -1 / 4]).as_matrix())

    #Permet de joindre les positions
    def joins(self, position: List[Tuple[float, float, float]]) -> List[float]:
        return list(self.pate1.angles_pour_pos(numpy.array(position[0]))) + list(self.pate2.angles_pour_pos(numpy.array(position[1])))\
               + list(self.pate3.angles_pour_pos(numpy.array(position[2]))) + list(self.pate4.angles_pour_pos(numpy.array(position[3])))

#Fonction d'initialisation du simulateur
def init():
    """Initialise le simulateur
    
    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot
 
 #Donne les angles pour les moteurs
def setJoints(robot, joints):
    """Définis les angles cibles pour les moteurs du robot
    
    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])

#Lance une démonstration des mouvements du robot en mode demo
def demo(t, amplitude):
    """Démonstration de mouvement (fait osciller une patte)
    
    Arguments:
        t {float} -- Temps écoulé depuis le début de la simulation
    
    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
        float -- amplitude de l'oscillation
    """
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints

#Position de repos
def at_rest():
    pos1 = numpy.array([d * math.cos(math.pi * 1 / 4), d * math.sin(math.pi * 1 / 4), 6])
    pos2 = numpy.array([d * math.cos(math.pi * 3 / 4), d * math.sin(math.pi * 3 / 4), 6])
    pos3 = numpy.array([d * math.cos(math.pi * -3 / 4), d * math.sin(math.pi * -3 / 4), 6])
    pos4 = numpy.array([d * math.cos(math.pi * -1 / 4), d * math.sin(math.pi * -1 / 4), 6])
    return(pos1, pos2, pos3, pos4)

#Fonction qui permet de bouger la pate avant gauche du robot en fonction de x, y et z en mode leg_ik
def leg_ik(x_aim, y_aim, z_aim):
    quadruped = Quadrupede()
    "On le met en position de repos"
    pos1, pos2, pos3, pos4 = at_rest()
    "On lui indique la position à adopter"
    pos1 = numpy.array([x_aim,y_aim,z_aim])
    return quadruped.joins((pos1,pos2,pos3,pos4))

#Fonction qui permet de bouger le corps du robot en fonction de x,y et z en mode robot_ik
def robot_ik(x_aim, y_aim, z_aim):
    quadruped = Quadrupede()
    pos1,pos2,pos3,pos4 = at_rest()
    pos = numpy.array([x_aim,y_aim,z_aim])
    pos1 -= pos
    pos2 -= pos
    pos3 -= pos
    pos4 -= pos
    return quadruped.joins((pos1,pos2,pos3,pos4))

#Fonction d'interpolation qui donne la valeur en y du point x à partir de X et Y
def interpolation(X,Y,x):
    sum = 0
    for i in range(len(X)):
        p = 1
        for j in range(len(X)):
            if j != i:
                p *= (x-X[j])/(X[i]-X[j])
        sum += Y[i] * p
    return sum

#Fonction qui fait marcher le robot à une vitesse x_speed et y_speed
def cycle_marche(time,x_speed,y_speed):
    t = time - int(time)
    X = 0
    Y = 0
    Z = 0
    ts = [0,0.25,0.5,0.75,1]
    zs = [0,AMPLITUDE,0,0,0]
    if x_speed != 0:
        xs = [0,x_speed*0.25,x_speed*0.5,x_speed*0.25,0]
        X = interpolation(ts,xs,t)
        Z = interpolation(ts,zs,t)
    if y_speed != 0:
        ys = [0,y_speed*0.25,y_speed*0.5,y_speed*0.25,0]
        Y = interpolation(ts,ys,t)
        Z = interpolation(ts,zs,t)  
    return numpy.array([X,Y,Z])

#Fonction qui fait tourner le robot sur lui-même à une vitesse t_speed tout en ayant la position de repos
def t_turn(time,t_speed,pos):
    t = time - int(time)
    angle = [math.pi*1/4,math.pi*3/4,math.pi*-3/4,math.pi*-1/4]
    ts = [0,0.25,0.5,0.75,1]
    theta = [0,t_speed/2,0,-t_speed/2,0]
    zs = [0,AMPLITUDE,AMPLITUDE,0,0]
    X = d*math.cos(angle[pos]+interpolation(ts,theta,t))
    Y = d*math.sin(angle[pos]+interpolation(ts,theta,t))
    Z = 6
    if t_speed != 0:
        Z = interpolation(ts,zs,t)
    return numpy.array([X,Y,Z])
    
#Fonction qui fait marcher le robot à la vitesse x_speed,y_speed et t_speed en mode walk
def walk(time,x_speed,y_speed,t_speed):
    quadruped = Quadrupede()
    x_speed *= 100
    y_speed *= 100
    pied1 = cycle_marche(time,x_speed,y_speed) + t_turn(time,t_speed,0)
    pied3 =  cycle_marche(time,x_speed,y_speed) + t_turn(time,t_speed,2)
    time += 0.5
    pied2 = cycle_marche(time,x_speed,y_speed) + t_turn(time,t_speed,1)
    pied4 = cycle_marche(time,x_speed,y_speed) + t_turn(time,t_speed,3)
    return quadruped.joins((pied1, pied2, pied3, pied4))

#Fonction qui fait marcher le robot pour qu'il rejoigne la position (x,y,z) et se tourne de t_angle en mode goto
def goto(time,x,y,t_angle):
    x_speed = 0.05
    y_speed = -0.05
    t_speed = 0.1
    if x<0:
        y_speed *= -1
    if y<0:
        y_speed *= -1
    if t_angle < 0:
        t_speed *= -1
    a = abs(x/(x_speed))
    b = abs(y/(y_speed))
    c =  abs(t_angle/t_speed)+max(a,b)
    if time > a:
        x_speed = 0
    if time > b:
        y_speed = 0
    if x_speed != 0 or y_speed != 0 or time > c:
        t_speed = 0
    return walk(time,y_speed,x_speed,t_speed)

#Fonction qui fait faire des pompes au robot d'amplitude p_a et à la vitesse v
def pump_func(time,v,p_a):
    Z = 0
    if v != 0 and p_a != 0:
        tempo = p_a/v
        time = time%tempo
        ts = [0,tempo/4,tempo/2,3*tempo/4,tempo]
        zs = [0,p_a/2,p_a,p_a/2,0]
        Z = interpolation(ts,zs,time)
    X = 0
    Y = 0
    return numpy.array([X,Y,Z])


#Fonction en mode fun
def fun(time,v,p_a):
    quadruped = Quadrupede()
    pied1 = numpy.array([2,12,6])+pump_func(time,v,p_a)
    pied3 = numpy.array([-22.4, -2, 0])
    pied2 =numpy.array([-22.4, 2, 0])
    pied4 = numpy.array([2,-12,6]) +pump_func(time,v,p_a)
    return quadruped.joins((pied1, pied2, pied3, pied4))

#Fonction main
if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, t_angle = args.m, args.x, args.y, args.t
    
    if mode not in ['demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)
    
    #Installe des sliders dans le simulateur
    robot = init()
    if mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')
    elif mode == 'leg_ik':
        x_slider = p.addUserDebugParameter("x_slider", -19, 21, 12)
        y_slider = p.addUserDebugParameter("y_slider", -19, 21, 12)
        z_slider = p.addUserDebugParameter("z_slider", -20, 20, 5)
        print('Mode de mouvement de la jambe...')
    elif mode == 'robot_ik':
        x_robot_slider = p.addUserDebugParameter("x_slider", -20, 20, 0)
        y_robot_slider = p.addUserDebugParameter("y_slider", -20, 20, 0)
        z_robot_slider = p.addUserDebugParameter("z_slider", -20, 20, 0)
        print('Mode de marche')
    elif mode == 'walk':
        x_speed_slider = p.addUserDebugParameter("x_speed_slider", -0.3, 0.3, 0)
        y_speed_slider = p.addUserDebugParameter("y_speed_slider", -0.3, 0.3, 0)
        t_speed_slider = p.addUserDebugParameter("t_speed_slider", -math.pi/3, math.pi/3, 0)
    elif mode == 'goto':
        print('Mode marche guidée')
    elif mode == 'fun':
        v_speed = p.addUserDebugParameter("pump_speed_slider",0,13,0)
        pump_amp = p.addUserDebugParameter("amplitude_pump_slider",0,10,6)
        print('Mode fun')
    else:
        raise Exception('Mode non implémenté: %s' % mode)
    
    t = 0

    # Boucle principale
    while True:
        t += dt

        if mode == 'demo':
            # Récupération des positions cibles
            joints = demo(t, p.readUserDebugParameter(amplitude))

        if mode == 'leg_ik':
            joints = leg_ik(p.readUserDebugParameter(x_slider), p.readUserDebugParameter(y_slider), p.readUserDebugParameter(z_slider))

        if mode == 'robot_ik':
            joints = robot_ik(p.readUserDebugParameter(x_robot_slider), p.readUserDebugParameter(y_robot_slider), p.readUserDebugParameter(z_robot_slider))

        if mode == 'walk':
            joints = walk(t,p.readUserDebugParameter(x_speed_slider), p.readUserDebugParameter(y_speed_slider), p.readUserDebugParameter(t_speed_slider))

        if mode == 'goto':
            joints = goto(t,x,y,t_angle)

        if mode == 'fun':
            joints = fun(t,p.readUserDebugParameter(v_speed),p.readUserDebugParameter(pump_amp))
        # Envoi des positions cibles au simulateur
        setJoints(robot, joints)

        # Mise à jour de la simulation
        p.stepSimulation()
        sleep(dt)
