# Quadruped

Quadruped est un projet Python qui permet de simuler le fonctionnement d'un robot à quatre pattes (d'où quadruped).
Il a pour but de prendre en main la manipulation d'un robot à partir des cerveaux moteurs et d'avoir une première vision de l'aspect robotique en informatique.
Les mouvements des robots n'étaient au départ que controlaient par les cerveaux moteurs du robot (3 par patte) et ne prenait qu'en entrée des angles (un par cerveau moteur).

## Installation

Il faut installer les dépendances suivantes avec pip par exemple:
```bash
pip install numpy scipy pybullet jupyter matplotlib
```

## Utilisation

Pour lancer le programme, il suffit de lancer la commande suivante :
```bash
python quadruped.py -m mode [-x x-cible -y y-cible -t t-cible]
```

Les différents modes implémentés dans ce projet sont :
* *demo*: ce mode fait une démonstration des mouvements du robot
* *leg_ik*: ce mode permet de bouger la patte avant gauche en fonction des curseurs (x,y,z) (en cm) dans le simulateur
* *robot_ik*: ce mode permet de bouger le corps du robot en fonction des curseurs (x,y,z) (en cm) dans le simulateur
* *walk*: ce mode implémente la marche du robot à une vitesse dans la direction x et y réglable grâce au curseur (x_speed,y_speed) (en m/s) et la rotation du robot selon l'axe z en fonction de theta_speed (en rad/s)
* *goto*: ce mode permet au robot de marcher dans une direction x et y et en se tournant d'un angle t. Ce mode nécessite cependant de remplir les paramètres optionnels de la commande ci-dessus (par défaut il avancera d'un mètre selon l'axe x).
* *fun*: ce mode va faire une démonstration d'un mouvement amusant ce sera la surprise.

## Quelques explications sur l'implémentations

L'implémentation du projet a nécessité de faire une conversion des positions (x,y,z) en angles(sigma1,sigma2,sigma3) pour chaque patte. 
Il a fallu alors délimiter les solutions des pattes. On va parler d'une spère de centre l'origine de la patte (où *épaule* du robot) et de rayon la longueur de la patte. En effet, la position d'une patte pouvait avoir:
* *0 solution*: si on demande une solution à l'extérieur de la sphère. Pour régler ce problème, il a juste fallu vérifier que la position donnée à la patte du robot était inférieur à la taille de la patte. Dans le cas contraire, on renvoie les angles 0,0,0 ce qui fait que la patte fait un mouvement brusque.
* *1 solution*: si on a le bout de la patte exactement sur la sphère. Il n'y avait pas de problème à régler pour celui-ci.
* *2 solition*: dans la plupart des cas où l'on se trouve à l'intérieur de la sphère. Il a juste fallu utiliser un arctan2 et limiter les solutions entre -2pi/3 et 2pi/3 (car les pattes vont se toucher sinon).
* *une infinité de solution*: si on demande une position sur la droite entre le robot et le premier cerveau moteur. Il a fallu contenir les valeurs dans l'ensemble de définition de arccos et arcsin c'est-à-dire entre -1 et 1.

Il a fallu faire des changements de repère pour passer du repère de la patte au repère du corps du robot et inversement.
Puis pour définir un mouvement de marche, il a fallu donner des points et interpoler cet ensemble de point afin de savoir où positionner la patte. Pour augmenter la vitesse du robot, c'est l'amplitude du mouvement de la patte qui grandit.
Quand au mouvement du robot pour une position donnée, il a fallu le faire marcher à une vitesse précise pendant un temps donné.

Une remarque: Il y a un certain décalage avec le mouvement du robot. Je n'ai pas ajouté de correcteur pour corriger cette déviation. On le voit plus précisément lorsqu'on demande au robot de se déplacer en mode goto où il lui manque quelques centièmtres pour arriver à sa destination finale.

## Autheur
HERTAY Dylan